<?php
namespace Home\Controller;
use Home\Service\YoCasService;
use Think\Controller;
class IndexController extends Controller {
    public function index(){

        $this->display();
    }

    /**
     * @throws \Exception
     */
    public function login(){
        $casServer = new YoCasService();
        $casServer->checkRedirectToCas();

        $this->display();
    }

    /**
     * 退出操作
     */
    public function logout(){
        $casServer = new YoCasService();
        $casServer->logout();
    }

}